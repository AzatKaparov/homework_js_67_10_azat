import MainContainer from "./containers/MainContainer";
import './App.css';

function App() {
  return (
    <div className="container">
      <MainContainer/>
    </div>
  );
}

export default App;
