const initialState = {
    password: '1987',
    enteredValue: '',
    rightPassword: null,
};

const checkPassword = (attempt) => {
    return attempt === initialState.password;
}

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case "ADD_LETTER":
            if (action.letter !== "E") {
                return {...state,
                    enteredValue:( state.enteredValue + action.letter).slice(0, 4),
                };
            } else {
                return {
                    ...state,
                    rightPassword: checkPassword(state.enteredValue),
                };
            }
        case "REMOVE_LETTER":
            return {
                ...state,
                enteredValue: state.enteredValue.slice(0, -1),
            };
        default:
            return state;
    }
};

export default reducer;