import React from 'react';
import './PanelItem.css';
import {useDispatch} from "react-redux";

const PanelItem = ({value}) => {
    const dispatch = useDispatch();
    const addLetter = () => dispatch({type: "ADD_LETTER", letter: value});
    const removeLetter = () => dispatch({type: "REMOVE_LETTER"});

    return (
        <button className="PanelItem" onClick={value === "<"
            ? removeLetter
            : addLetter
        }>
            {value}
        </button>
    );
};

export default PanelItem;