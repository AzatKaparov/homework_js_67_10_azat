import React from 'react';
import {useSelector} from "react-redux";
import './Display.css';

const Display = () => {
    const inpVal = useSelector(state => state.enteredValue);
    const right = useSelector(state => state.rightPassword)

    let extraClass = "";
    if (right === true) {
        extraClass = "Display-success";
    } else if (right === false) {
        extraClass = "Display-wrong";
    }

    return (
        <>
            <input onChange={e => e.preventDefault()}
                   placeholder="Enter password"
                   className={["Display", extraClass].join(" ")}
                   name="password"
                   type="password"
                   value={inpVal}>
            </input>
        </>

    );
};

export default Display;