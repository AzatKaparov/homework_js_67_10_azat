import React from 'react';
import {useSelector} from "react-redux";
import './MainContainer.css';
import Display from "../components/Display/Display";
import PanelItem from "../components/PanelItem/PanelItem";

const MainContainer = () => {
    const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
    const right = useSelector(state => state.rightPassword)

    return (
        <div className="panel-block">
            {right === true && <h1>Access Granted</h1>}
            {right === false && <h1>Access Denied</h1>}
            <Display/>
            {numbers.map(item => (
                <PanelItem key={item} value={item} />
            ))}
            <PanelItem value="<" />
            <PanelItem value="E" />
        </div>
    );
};

export default MainContainer;